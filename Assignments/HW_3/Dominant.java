import java.util.Arrays;

public class Dominant {
    static int dominant(int[] a) {
        int[] b = {-1, 0};
        for (int i = 0; i < a.length; i++) {
            if (b[1] == 0) {
                b[0] = a[i];
                b[1] = 1;
            } else {
                if (a[i] == b[0]) {
                    b[1] += 1;
                } else {
                    b[1] -= 1;
                    if (b[1] == 0) {
                        b[0] = -1;
                    }
                }
            }
        }
        return b[0];
    }
    public static void main(String[] args) {
        int[] a = {1, 2, 3};
        System.out.println(dominant(a));
    }
}
    

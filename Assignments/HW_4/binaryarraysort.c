#include <stdio.h>
#include <stdlib.h>

void sort(int *, int, int);
void print(int *, int);

int main(void) {

    int a[] = {1,0,1,1,0,0,0,1,1};
    int size = sizeof(a) / sizeof(a[0]);
    print(a, size);
    int i;
    int zero_counter = 0;
    int one_counter = 0;
    for (i = 0; i < size; i++) {
        if (a[i] == 0) {
            zero_counter += 1;
        } else {
            one_counter += 1;
        }
    }
    sort(a, zero_counter, one_counter);
    print(a, size);

}

void sort(int * a, int zero, int one) {
    int i;
    for (i = 0; i < zero; i++) {
        *(a + i) = 0;
    }
    int j;
    for (j = 0; j < one; j++) {
        *(a + j + zero) = 1;
    }
}

void print(int * a, int size) {
    int i;
    for (i = 0; i < size; i++) {
        printf("%d ", *(a + i));
    }
    printf("\n");
}

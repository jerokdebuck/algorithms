#include <stdio.h>

int binarySearch(int, int *, int, int);
void print(int *);

int main(void) {

    int a[] = {5, 2, 6, 8, 9, 18, 12, 20, 21, 11};
    int b[] = {1, 3, 5, 7, 9, 11, 13, 15, 17, 19};
   
    int i;
    int found = 0;
    for (i = 0; i < sizeof(a) / sizeof(a[0]); i++) {
        int result = binarySearch(a[i], b, 0, (sizeof(b)/sizeof(b[0])) - 1);
        if (result != -1) {
            a[found] = result;
            found += 1;
        }
    }
    
    int c[found];
    int j;
    for(j = 0; j < found; j++) {
        c[j] = a[j];
    }
    print(c);
    return 0;
}

void print(int * a) {
    int size = sizeof(a) / sizeof(a[0]);
    int i;
    for (i = 0; i <= size; i++) {
        printf("%d ", *(a + i));
    }
    printf("\n");
}

int binarySearch(int treasure, int * b, int begin, int end) {
    if (begin < end) {
        int mid = (begin + end) / 2;
        if (*(b + mid) == treasure) {
            return treasure;
        } else if (*(b + mid) > treasure) {
            binarySearch(treasure, b, 0, mid - 1);
        } else {
            binarySearch(treasure, b, mid + 1, end);
        }
    }
    else {
        return -1;
    }
}
    



#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void absolute(int*);

int main(void) {
    srand(time(NULL));
    int a[20];
    int loop;
    for (loop = 0; loop < sizeof(a) / sizeof(a[0]); loop++) {
        a[loop] = rand() % 20;
    }
    int max = 0;
    
    int i;
    for (i = 0; i < sizeof(a) / sizeof(a[0]); i++)  {
        if (a[i] > max) {
            max = a[i];
        }
    }
    printf("Max is: %d\n", max);
    int min = max;
    int j;
    for (j = 0; j < sizeof(a) / sizeof(a[0]); j++) {
        if (a[j] < min) {
            min = a[j];
        }
    }
    printf("Min is: %d\n", min);

    int k;
    int b[max - min + 1];

    for (k = 0; k < max - min + 1; k++ ) {
        b[k] = min + k;
    }
    double mean = (min + max) / 2.0;
   
    int minmax[sizeof(b) / sizeof(b[0])]; 
    int umin[sizeof(b) / sizeof(b[0])];

    for (i = 0; i < sizeof(b) / sizeof(b[0]); i++) {
        int c[sizeof(a) / sizeof(a[0])];
        int max_set = 0;
        for (j = 0; j < sizeof(a) / sizeof(a[0]); j++) {
            c[j] = a[j] - b[i];
            int * x = &c[j];
            absolute(x);
            if (c[j] > max_set) {
                max_set = c[j];
            }
            printf("a%d = %d, u%d = %d, value is : %d\n", j, a[j], i, b[i], c[j]);
        }
        printf("Maximum of that set was : %d\n", max_set);
        minmax[i] = max_set;
        umin[i] = b[i];
        printf("\n");
    }
    int min2 = max;
    int uminfind = -1;
    for (i = 0; i < sizeof(minmax) / sizeof(minmax[0]); i++) {
        if (minmax[i] < min2) {
            min2 = minmax[i];
            uminfind = umin[i]; 
        }
    }
    printf("minimum of the maximum is: %d at u = %d\n", min2, uminfind);
    printf("Mean of max + min = %f\n", mean);
    return 0;
}

void absolute(int * x) {
    if (*x < 0) {
        *x *= -1;
    }
}

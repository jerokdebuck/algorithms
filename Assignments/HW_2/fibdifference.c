
#include <stdio.h>
#include <stdlib.h>

int fibone(int);
int fibtwo(int);

int main(void) {
    int x;
    printf("Enter integer: ");
    scanf("%d",&x);
    int y = fibone(x);
    printf("\nBad fib(%d) = %d\n", x, y);
    int z = fibtwo(x);
    printf("Good fib(%d) = %d\n", x, z);
    return 0;
}

int fibone(int n) {
    if (n <= 2) {
        return 1;
    } else {
        return fibone(n - 1) + fibone(n - 2);
    }
}

int fibtwo(int n) {
    int b[n];
    b[0] = b[1] = 1;
    
    int i;
    for (i = 2; i < n; i++) {
        b[i] = b [i - 1] + b[i - 2];
    }
    return b[n - 1];
} 

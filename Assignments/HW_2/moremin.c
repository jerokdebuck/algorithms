#include <stdio.h>
#include <stdlib.h>

int main(void) {

    int A[] = {1, 3, 6, 8, 12, 21, 22};
    int sizeA = sizeof(A) / sizeof(A[0]);
    int B[sizeA - 1];
    
    int i;
    for (i = 0; i < sizeA; i++) {
        if (i != sizeA) {
            B[i] = A[i + 1] - A[i];
        }
    }

    int j;
    for (j = 0; j < sizeof(B) / sizeof(B[0]); j++) {
        printf("%d ", B[j]);
    }
    printf("\n");

    return 0;
}

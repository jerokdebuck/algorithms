#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

bool function(int[], int, int);

int main(void) {
    
    //int a[] = {-5, 2, 3, 4, 5, 9, 11, 23, 45};
    int a[] = {-10, 2, 5, 7};
    int size = sizeof(a) / sizeof(a[0]);
    bool x = function(a, 0, size - 1);
    if (x) {
        printf("True\n");
    } else {
        printf("False\n");
    }
    return 0;
}

bool function(int a[], int p, int r) {
    bool found = false;
    if (p < r) {
        int q = (p + r) / 2;
        if (a[q] < q + 1) {
            function(a, q + 1, r);
        } else if (a[q] > q + 1) {
            function(a, p, q - 1);
        } else {
            found = true;
        }
    }
    return found;
}
